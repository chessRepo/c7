module UCICommunication where
import Data.Char (ord, chr)
import Data.List
import Control.Concurrent (forkIO, newEmptyMVar, MVar, takeMVar)
import Control.Monad.Writer.Lazy
import Control.Monad.Reader
import Control.Monad.Trans.Maybe
import Data.Time.Clock
import Data.Time.Format
import Data.Char
import Data.List.Split
import System.IO

import Helper
import Board

data ActionType = SendId | DoNothing | AnswerReadyOk | AnswerStop |AnswerBestMove |AnswerGoInfinite | UnrecognizedCommand deriving (Show)
data Action = ActionParameter { action_type :: ActionType, action_param :: String }
getLineW :: WriterT [String] IO String
getLineW = do
    line <- lift $getLine
    formatted <- lift(formatString "received " line)
    lift( appendFile logFile formatted)
    tell [formatted]
    return line
    
putLineW :: String -> WriterT [String] IO ()
putLineW str = do
    let s = str ++ "\n"
    lift (hSetBuffering stdout NoBuffering)
    lift (putStr s)    
    lift (putStrLn "")
    formatted <- lift(formatString "written " s)
    lift( appendFile logFile formatted)
    tell [formatted]

position_fen_pattern = "position fen"

takeActionType :: String -> ActionType
takeActionType guiCommand
    | "uci" `isPrefixOf` guiCommand = SendId
    | "debug" `isPrefixOf` guiCommand = DoNothing
    | "isready" `isPrefixOf` guiCommand = AnswerReadyOk
    | "stop" `isPrefixOf` guiCommand = AnswerStop
    | "position fen" `isPrefixOf` position_fen_pattern = AnswerBestMove
    | "go infinite" `isPrefixOf` guiCommand = AnswerGoInfinite
    | otherwise = UnrecognizedCommand

takeAction :: String -> Action
takeAction line = ActionParameter (takeActionType line) line
    
performAction :: Action -> WriterT [String] IO ()

performAction ActionParameter { action_type = AnswerStop }  = putLineW "uciok"
performAction ActionParameter { action_type = SendId } = do
    putLineW "id name checkmater 1 1.0" 
    putLineW "id author Marek"
    putLineW ""
    --putLineW "option name Ponder type check default false"
--    putLineW "option name Pozorna Opcja type button"
    putLineW "uciok"

--performAction DoNothing = putLineW "do nothing"
performAction ActionParameter { action_type = AnswerReadyOk } = putLineW "readyok"
--performAction UnrecognizedCommand = putLineW "unrecognized command"
performAction ActionParameter { action_type = AnswerBestMove, action_param =  input_line  } = do
    putLineW "super akcja"
    let input_data = tail . head . tail $ splitOn position_fen_pattern input_line
    putLineW "super akcja 2"
    putLineW input_data
    let b = parse input_data
    let c = showBoard (SimpleBoard b) ""
    putLineW c 
    putLineW "hello ello"

-- putLineW $"test " ++ a
    
--performAction _ = putLineW "nie wiem co robic"--return ()
--playGame mVar = do
--    positoin <- takeMVar mVar
--    testPositionChecking
    
logFile = "/home/marek/logs/chessLog"


formatString :: String -> String -> IO String
formatString prefix str = do
    now <- getCurrentTime    
    return (prefix ++ (formatTime defaultTimeLocale "%r %q" now) ++ " : " ++  str ++ "\n")

uciRead :: MVar Int -> IO ()
uciRead mVar = do
    line <- getLine
    if line == "koniec" then 
        return ()
    else do
        putStrLn "read from keyboard: "
        putStrLn line
        uciRead mVar

talkGui :: WriterT [String] IO ()
talkGui = do
    line <- getLineW
    performAction $ takeAction line
    if line == "quit" then 
        return ()
    else do talkGui

commandFen = "position fen rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"


makeAction = performAction ActionParameter { action_type = AnswerBestMove, action_param = commandFen }
