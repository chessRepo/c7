import Data.Char (ord, chr)
import Data.List
import Control.Concurrent (forkIO, newEmptyMVar, MVar, takeMVar)
import Control.Monad.Writer.Lazy
import Control.Monad.Reader
import Control.Monad.Trans.Maybe
import Data.Time.Clock
import Data.Time.Format
import Data.Char
import Data.List.Split
import System.IO

import GamePiece
import Board
import MoveBasic
import MoveCalculation
import UCICommunication
import Helper

    
        
main = do
    a <- runWriterT talkGui
    putStrLn $ head $ snd a
    appendFile "/home/marek/logs/chessLog" $ unwords $ snd a 
--    communicationVariable <- newEmptyMVar
--    t_positionChecking <- forkIO testPositionChecking
--    uciRead communicationVariable
    putStrLn "EndOfApp"
    
