module MoveCalculation where

import Control.Monad.Reader
import GamePiece
import Board
import MoveBasic

-- ten score - wywal!!!!! po co to ? Jak bedzie trzeba obliczyc wartosc pozycji to po prostu zmapuj !
-- next_states - dyskusyjne czy to potrzeba
-- move depth - number of move which is to be made on board
data GameState = GameState { board :: Board, move_depth :: Int, score :: Float, next_states :: Maybe [GameState] } deriving (Show)

maxDeepth = 4


assignStatesToState :: [GameState] -> GameState -> GameState
assignStatesToState nextMoves state = GameState (board state) (move_depth state) 0.0 (Just nextMoves)
simpleWhoMoves state = if odd (move_depth state) then W else B
whoMoves :: Reader GameState Team
whoMoves = reader $ simpleWhoMoves

-- calculates net sttes of given state and assigns those states to given state
addNextStates :: GameState -> GameState
addNextStates state = 
    let 
        nextMoves = runReader calculateNextMoves state
    in
        assignStatesToState nextMoves state

lastCheckingState state = (move_depth state) >= maxDeepth -1

 
getNextMoves :: Board -> Team -> [Board]
getNextMoves brd tm = do
    field_coordinate <- teamPieces tm (fieldInfo brd)
    
    -- for each possible moves from given field
    possible_new_field <- newPosition_getCoords <$> possibleMoves brd field_coordinate 

    return $ SimpleBoard $  movePiece2 (pieces brd) field_coordinate possible_new_field

-- refactor below code so that checking deepth level is done only in one function ...
calculateNextMoves :: Reader GameState [GameState]
calculateNextMoves = do 
    player <- whoMoves
    brd <- board <$> ask
    deepth <- move_depth <$> ask
    state <- ask
    let nextBoards = getNextMoves brd player -- for each board calculate further states
    let nextStates = map (\b2 -> GameState b2 (deepth+1) 0.0 Nothing) nextBoards -- stworz nowe stany
    -- for each of ^ check if it is worth considering, if so - dig in
    -- information to decide ^ should be provided as information of another scores
--            nextNextStates = map addNextStates nextStates -- dodaj stany podrzedne
    if lastCheckingState state then
        return nextStates
    else
        return $ map addNextStates nextStates

        
-- f scoresSoFar{deepth, score} GameState = nextPositions <- calculate
--                                          compare nextPositions with scoresSoFar                                            
--                                          update scoresSoFar
--                                          decide whether to go on, maybe calculate another scores
-- 


