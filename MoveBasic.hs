module MoveBasic where

import Data.Maybe

import Board
import GamePiece

data CanGoType = Free | Self | Take GamePiece | OutOfBoard deriving (Show, Eq)

data NewPosition = NewPosition FieldCoordinates CanGoType deriving (Show)

data CheckMater = CheckMater GamePiece

-- szach jest jesli ktorakolwiek figor szachuje
-- mat jest jesli jest szach, i nie istnieje taki ruch po ktorym nie ma szacha 

nPgetCoords :: NewPosition -> FieldCoordinates
nPgetCoords (NewPosition coords _) = coords
newPosition_getCoords = nPgetCoords

actionOnBoard :: Team -> Board -> FieldCoordinates -> CanGoType
actionOnBoard tm board coord
    | inBoard coord = let
            piece = fieldInfo board coord
            in case piece of
                      (Piece item tm_) -> if tm_ == tm then Self
                                          else Take piece
                      Empty            -> Free
    | otherwise = OutOfBoard

teamPieces :: Team -> (FieldCoordinates -> GamePiece) -> [FieldCoordinates]
teamPieces tm p = filter ( (==tm) . team . p ) $ filter ( (/=Empty) . p ) allCoordinates

isChecked :: Team -> Board -> [NewPosition]
isChecked tm brd = do -- TODO: tm opposite!
    c <- teamPieces tm (fieldInfo brd) -- coordinates of opposite team
    p <- possibleMoves brd c -- possible moves of piece 

    case p of
        (NewPosition _ (Take (Piece King tm))) -> return p
        otherwise -> mempty

isCheckedMaybe :: Team -> Board -> Maybe [NewPosition]
isCheckedMaybe tm brd = 
    let res = isChecked tm brd
    in if length res == 0 then Nothing else Just res
    

-- gets multiplied coords
getNewCoords :: MoveVector -> [Int] -> [MoveVector]
getNewCoords moveVector multipliers =
    let fs = map (mul) multipliers
    in fs <*> [moveVector]

mul :: Int -> (MoveVector -> MoveVector)
mul value = \(Coord r c) -> Coord (r * value) (c * value)

-- getMoves :: Board -> Team -> FieldCoordinates -> [FieldCoordinates]
-- getMoves board tm fields = do
--     let piece = fieldInfo board fields
    
--     -- if we want to get moves from field where there is no figure, then no moves are available
--     if piece == Empty then
--         mempty
--     else
--         do
--             let pieceTeam = team piece
--             -- if trying to get mvoes of opponnent piece then sorry - only tm's pieces can move
--             if pieceTeam /= tm then
--                 mempty
--             else
--                 possibleMoves board fields

canGoThere :: NewPosition -> Bool
canGoThere (NewPosition _ r )=
    case r of
        Free -> True
        (Take _ ) -> True
        otherwise -> False

multipliedVectors2 :: Item -> [MoveVector]
multipliedVectors2 item = do
    baseVector <- baseVectors item

    getNewCoords baseVector (getMultipliers item)

-- Gives possible new coordinates of piece which is at current location on the board.
-- List monad
possibleMoves :: Board -> FieldCoordinates -> [NewPosition]
possibleMoves board pieceCoords = do   
    let piece = fieldInfo board pieceCoords
    
    if piece == Empty then mempty
    else do
        let itemInstance = item piece
        
        baseVector <- baseVectors itemInstance
    
        let potentialCoordsFromBaseVector = add pieceCoords <$>  getNewCoords baseVector (getMultipliers itemInstance)
        
        let a = actionOnBoard (team piece) board -- [FieldCoordinates] -> [Action]
        let mp = (\x -> (NewPosition (x) (a x)))
        -- mp -> maps new coordinates and move info into NewPosition)
        -- span -> considers only moves up to the position where Piece captures another or goes over board
        possibleMove <- if itemInstance == Knight then
                            mp <$> potentialCoordsFromBaseVector
                        else
                            fst $ span canGoThere  $ mp <$> potentialCoordsFromBaseVector
        
	let teamTaken = team $ fieldInfo board (nPgetCoords possibleMove)
 
        if team piece == teamTaken then []
        else return possibleMove

add :: MoveVector -> MoveVector -> MoveVector
add (Coord r1 c1) (Coord r2 c2) = (Coord (r1 + r2) (c1 + c2))


bV1 :: MoveVector -> Int -> [MoveVector]        
bV1 vStart m = zipWith ($) fs (replicate (length fs) vStart)
    where
        fs = map mul [1..m]    
      
multipliedVectors :: [MoveVector] -> [MoveVector]
multipliedVectors vs = do
    v <- vs
    bV1 v 8

baseVectors :: Item -> [MoveVector]
baseVectors Rook = [(Coord 1 0), (Coord (-1) 0), (Coord 0 1), (Coord 0 (-1))]
baseVectors Knight = [(Coord 2 1), (Coord 2 (-1)), (Coord (-2) 1), (Coord (-2) (-1))]
baseVectors Bishop = [(Coord 1 1), (Coord 1 (-1)), (Coord (-1) 1), (Coord (-1) (-1))]
baseVectors Queen = (baseVectors Bishop) ++ (baseVectors Rook)
baseVectors Pawn = [(Coord 1 0)]
baseVectors King = baseVectors Queen

getMultipliers :: Item -> [Int]
getMultipliers Rook = [1..8]
getMultipliers Bishop = [1..8]
getMultipliers Queen = [1..8]
getMultipliers King = [1]
getMultipliers Knight = [1]
getMultipliers Pawn = [1]

-- moves piece from given coordinates to another
movePiece2 ::  [[GamePiece]] -> FieldCoordinates -> FieldCoordinates -> [[GamePiece]]
movePiece2 pcs (Coord rowFrom colFrom) (Coord rowTo colTo) = result
    where
        rFrom = pcs !! rowFrom
        pieceFrom = rFrom !! colFrom
        newRowFrom = replaceCol rFrom colFrom Empty
        piecesWithReplacedRowFrom = replaceCol pcs rowFrom newRowFrom
        rTo = piecesWithReplacedRowFrom !! rowTo
        newRowTo = replaceCol rTo colTo pieceFrom  
        result = replaceCol piecesWithReplacedRowFrom rowTo newRowTo

replaceCol :: [a] -> Int -> a -> [a]
replaceCol items idx newVal = prefix ++ [ newVal ] ++ suffix
    where
        (prefix, (_:suffix)) = splitAt idx items 
