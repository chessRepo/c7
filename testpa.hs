import Data.Char (ord, chr)
import Data.List
import Control.Concurrent (forkIO, newEmptyMVar, MVar, takeMVar)
import Control.Monad.Writer.Lazy
import Control.Monad.Reader
import Control.Monad.Trans.Maybe
import Data.Time.Clock
import Data.Time.Format
import Data.Char
import Data.List.Split
import System.IO

import GamePiece
import Board
import MoveBasic
import MoveCalculation
import UCICommunication
import Helper

    
        
main = do
    a <- runWriterT makeAction
    putStrLn $ head $ snd a
    putStrLn "EndOfApp"
    
