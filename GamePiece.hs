module GamePiece where 

data Team = W | B deriving (Show, Eq)

data Item = Pawn | Knight | Bishop | Rook | Queen | King  deriving (Show, Eq)

data GamePiece = Piece { item :: Item, team ::  Team } | Empty deriving (Show, Eq)


showPiece :: GamePiece -> String
showPiece (Piece _ t) = show t
showPiece Empty     = "_"

whitePiece item = Piece item W -- note: first put arguments which aren't going to change throught computations
blackPiece item = Piece item B


scorePiece :: Item -> Float
scorePiece piece = case piece of
    Pawn -> 0.2
    Knight -> 2.5
    Bishop -> 3
    Rook -> 5
    Queen -> 9
    King -> 99999 
    otherwise -> 12
