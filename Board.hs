module Board where
import Data.Char (ord, chr)

import GamePiece

data Board = SimpleBoard { pieces :: [[GamePiece]] } deriving (Show)

data FieldCoordinates = Coord { row :: Int, col :: Int } | ChessNotation { coords :: String } deriving (Show)
type MoveVector = FieldCoordinates

testingBoard = 
    [whitePiece Rook, Empty, Empty]
    : [whitePiece Bishop, Empty, Empty]
    : [Empty, blackPiece Rook, Empty] 
    : []
   
boardSize :: Board -> Int
boardSize brd = rowLength
    where
        p = pieces brd
        rowLength = length . head $ p

-- gives information about which piece occupies given position
fieldInfo :: Board -> FieldCoordinates -> GamePiece
fieldInfo board coordinates = let
    rr = (pieces board) !! (row coordinates)
    figure = rr !! (col coordinates)
    in figure


makeBoard = SimpleBoard $ startBoard

allCoordinates :: [FieldCoordinates] -- list of all fields on chessboard
allCoordinates = do
    let takeFields = take (boardSize makeBoard) 
    row_s <- takeFields $  map show [1..8]
    col_s <- takeFields $ map (:"") ['a' .. 'z']
    return $ toCoord . ChessNotation  $ col_s ++ row_s    




-- conversion between different FieldCoordinates instances
toCoord :: FieldCoordinates -> FieldCoordinates
toCoord (ChessNotation coords) = let
    first = coords !! 0
    second = coords !! 1
    colN = (ord first) - (ord 'a')
    rowN = (ord second) - (ord '1')
    in (Coord rowN colN)
    
toChess :: FieldCoordinates -> FieldCoordinates
toChess (Coord r c) = let
    rowChar = chr ((ord 'a') + c)
    colChar = chr ((ord '1') + r)
    in ChessNotation ([rowChar] ++ [colChar])




-- checks if given coordinates are within the board
inBoard :: FieldCoordinates -> Bool
inBoard (Coord row col) = row >= 0 && row <=  maxIndex && col >= 0 && col <= maxIndex
    where
        maxIndex = (boardSize makeBoard) - 1
    
    

firstLine = [Rook, Knight, Bishop, Queen, King, Bishop, Knight, Rook]
--secondLine = take 8 (repeat Pawn)
secondLine = take 8 (repeat Pawn)
emptyLine = take 8 (repeat Empty)

emptyLines = concat $ take 4 (repeat emptyLine)


startBoard :: [[GamePiece]]
startBoard = 
    (map whitePiece firstLine) 
    : emptyLine -- : (map whitePiece secondLine)
    : (replicate 4 emptyLine)
    ++ (
        emptyLine -- (map blackPiece secondLine)
        : (map blackPiece firstLine)
        : []
        )
