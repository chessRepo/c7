module Helper where
import Data.Char (ord, chr)
import Data.List
import Control.Concurrent (forkIO, newEmptyMVar, MVar, takeMVar)
import Control.Monad.Writer.Lazy
import Control.Monad.Reader
import Control.Monad.Trans.Maybe
import Data.Time.Clock
import Data.Time.Format
import Data.Char
import Data.List.Split
import System.IO

import GamePiece
import Board
import MoveBasic
import MoveCalculation

-- d1 = row = 0, col = 1
-- pieces - pierwsza lista zawiera wiersz pierwszy!
-- row is a, b c..h col - 1,2 .. 8

maxLevel = 15

test1 :: [[GamePiece]] -> FieldCoordinates -> Int
test1 pcs (Coord r c) = r

getLines state = mapLines pcs
    where
        pp  = take (2 + (move_depth state)) (repeat ' ')
        pcs = reverse $ pieces $ board state
        mapLine x = pp ++ (concat (map showPiece x)) ++ ['\n'] 
        mapLines x = concat (map mapLine x)

showBoard brd prefix = mapLines pcs
    where
        pcs = reverse $ pieces $ brd
        mapLine x = prefix ++ (concat (map showPiece x)) ++ ['\n'] 
        mapLines x = concat (map mapLine x)
 
-- TODO monad transforomers !!
printState :: GameState ->  IO () 
printState state = do
    let pp  = take (move_depth state) (repeat ' ')
    putStrLn  $ pp ++ "Move deepth: " ++ (show ( move_depth state))
--    putStrLn $ (getLines state) putStrLn $ pp ++"Child states:"

    case (next_states state) of
        Nothing -> return ()
        Just ns -> sequence_ $ map printState ns

 --   nextStates <- next_states state
 --   sequence_ printState nextStates
 --   lift $ putStrLn "hello"

pp :: Maybe [GameState] -> IO ()
pp (Just states) = do
    sequence $ map printState states
    return mempty
    
calculateBestMove :: GameState -> (GameState -> GameState -> Ordering) -> [GameState]
--calculateBestMove :: Reader GameState [GameState]
calculateBestMove state sortMethod = do
    let nextMoves = runReader calculateNextMoves state
    -- chose locally best moves to analyze
    let mappedMinimalPosition = sortBy sortMethod nextMoves

    take 10 mappedMinimalPosition
    
testState = (GameState makeBoard 1 0.0 Nothing)
testFunction = runReader calculateNextMoves testState

emptyFieldRepresentation = '@'

       
testPieces :: Board -> [GamePiece]
testPieces board = concat (pieces board)

charToPiece :: Char -> GamePiece
charToPiece '@' = Empty
charToPiece c = Piece figure fteam 
    where 
        figure = case toLower c of
                'q' -> Queen
                'k' -> King
                'b' -> Bishop
                'n' -> Knight
                'r' -> Rook
                'p' -> Pawn
        fteam = if isLower c then B else W 

parse :: String -> [[GamePiece]]
parse input_str = do
    let str = head . splitOn " " $ input_str -- take only first part of the string
    oneLine <- splitOn "/" str
    let normalLine = changeLine2 oneLine
    return $ map charToPiece normalLine

changeLine2 line = do
    c <- line
    if isNumber c then
        replicate (digitToInt c) emptyFieldRepresentation 
    else
        return c
        
testPositionChecking = do
    putStrLn "scorePiece Pawn"
    putStrLn .show $ scorePiece Pawn
    putStrLn "scorePiece King"
    putStrLn .show $ scorePiece King
    putStrLn "\nStartBoard:\n"
    putStrLn . show $ makeBoard
    putStrLn . show $ (toCoord (ChessNotation "c4"))
    
    putStrLn "Coord c1"
    putStrLn . show $ (toCoord (ChessNotation "c1"))
    putStrLn "Item at b2"
    putStrLn . show $ fieldInfo (makeBoard) (toCoord (ChessNotation "b2"))
    putStrLn "Item at c1"
    putStrLn . show $ fieldInfo (makeBoard) (toCoord (ChessNotation "c1"))
    putStrLn "Possible moves of c1"
    putStrLn . show $ fmap toChess $ newPosition_getCoords <$> possibleMoves (makeBoard) (toCoord (ChessNotation "h8"))
    putStrLn "Possible moves of c1"
    putStrLn . show $ fmap toChess $ newPosition_getCoords <$> possibleMoves (makeBoard) (toCoord (ChessNotation "c2"))
    putStrLn "Base vectors: Bishop"
    putStrLn . show $ baseVectors Bishop
    
        
